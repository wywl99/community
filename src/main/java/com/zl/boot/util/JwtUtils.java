package com.zl.boot.util;

import cn.hutool.jwt.JWTUtil;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public class JwtUtils {

    public static final String SALT = "zhang_lun";

    public static String getToken(Map<String,Object> map){
        map.put("expire_time", System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 30);

        return JWTUtil.createToken(map,SALT.getBytes(StandardCharsets.UTF_8));
    }

}
