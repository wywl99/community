package com.zl.boot.util;


import com.zl.boot.domain.ResultData;

/**
 * 返回工具类
 */
public class ResultUtils {

    /**
     * 账号或者密码错误
     */
    public static final int AUTHENTICATION_ERROR = 10010;

    private ResultUtils(){}

    public static ResultData success(){
        return new ResultData(0,null,null);
    }

    public static ResultData success(Object data){
        return new ResultData(0,null,data);
    }

    public static ResultData success(int code, String msg, Object data){
        return new ResultData(code, msg, data);
    }

    public static ResultData success(int code, Object data){
        return new ResultData(code, null,data);
    }
    public static ResultData error(int code, String msg){
        return new ResultData(code,msg,null);
    }

    public static ResultData error(int code){
        return new ResultData(code,null,null);
    }

    public static ResultData error(String msg){
        return error(-1,msg);
    }

}
