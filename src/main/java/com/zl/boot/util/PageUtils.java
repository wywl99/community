package com.zl.boot.util;




import com.zl.boot.domain.vo.PageVO;

import java.util.Collection;
import java.util.Collections;

/**
 * 分页工具类
 */
public class PageUtils {

    public static <T> PageVO<T> convert(PageVO<?> source, Collection<T> data){

        PageVO<T> pageVO = new PageVO<>();
        pageVO.setPage(source.getPage());
        pageVO.setData(data);
        pageVO.setPageCount(source.getPageCount());
        pageVO.setTotal(source.getTotal());

        return pageVO;
    }

    public static <T> PageVO<T> emptyPage(){

        PageVO<T> pageVO = new PageVO<>();

        pageVO.setData(Collections.emptyList());
        pageVO.setPage(1);
        pageVO.setPageCount(0);
        pageVO.setTotal(0L);

        return pageVO;
    }
}