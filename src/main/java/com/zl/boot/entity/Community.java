package com.zl.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
@Data
public class Community implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    /**
     * 经度

     */
    private Double longitude;

    /**
     * 纬度
     */
    private Double latitude;

    private String remark;

    /**
     * 风险等级
     */
    private Integer risk;

    /**
     * 密接人数
     */
    private Integer contact;

    /**
     * 确诊人数
     */
    private Integer confirmed;

}
