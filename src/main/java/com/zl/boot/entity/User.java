package com.zl.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private String username;

    private String password;

    private String nickname;

    private String mobile;

    /**
     * 疫苗接种状态
     */
    private Integer vaccineState;

    private Integer community;

    /**
     * 0是普通用户
     * 1是管理员
     */
    private Integer role;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime time;

    /**
     * 风险等级
     * 0正常 1密接 2确诊
     */
    private Integer risk;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 身份证号码
     */
    private String card;

    /**
     * 头像
     */
    private String header;
}
