package com.zl.boot.mapper;

import com.zl.boot.entity.Vaccine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
public interface VaccineMapper extends BaseMapper<Vaccine> {

}
