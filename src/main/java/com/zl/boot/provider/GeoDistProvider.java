package com.zl.boot.provider;

import com.zl.boot.domain.MyPoint;
import com.zl.boot.domain.dto.RadiusDTO;
import com.zl.boot.entity.Community;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.GeoOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.domain.geo.Metrics;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 距离计算
 */
@Component
@Slf4j
public class GeoDistProvider {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Value("${my.dist}")
    private Integer dist;

    public int dist(Point source, Point target){

        GeoOperations<String, String> geoOperations = redisTemplate.opsForGeo();

        String key = UUID.randomUUID().toString();

        geoOperations.add(key, source, "geo1");
        geoOperations.add(key, target, "geo2");

        Distance distance = geoOperations.distance(key, "geo1", "geo2",Metrics.KILOMETERS);

        return (int) distance.getValue();
    }

    public List<RadiusDTO> getByRadius(Point point){

        GeoOperations<String, String> operations = redisTemplate.opsForGeo();

        Circle circle = new Circle(point,new Distance(dist,Metrics.KILOMETERS));

        RedisGeoCommands.GeoRadiusCommandArgs limit = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs()
                .sortAscending()
                .includeDistance();

        GeoResults<RedisGeoCommands.GeoLocation<String>> city = operations.radius("city", circle,limit);

        Iterator<GeoResult<RedisGeoCommands.GeoLocation<String>>> iterator = city.iterator();

        List<RadiusDTO> list = new ArrayList<>();

        while (iterator.hasNext()) {

            RadiusDTO radiusDTO = new RadiusDTO();

            GeoResult<RedisGeoCommands.GeoLocation<String>> next = iterator.next();

            String name = next.getContent().getName();
            double value = next.getDistance().getValue();

            String[] split = name.split("-");

            radiusDTO.setId(Integer.valueOf(split[1]));
            radiusDTO.setDist(value);
            list.add(radiusDTO);
        }

        return list;
    }

    /**
     * 添加一个点
     */
    public void addPoint(Point point,Object id){
        redisTemplate.opsForGeo().add("city", point, UUID.randomUUID().toString().replaceAll("-", "") + "-" + id);
    }

}
