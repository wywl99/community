package com.zl.boot.provider;


import com.zl.boot.entity.User;

    public class UserContext {

    private static final ThreadLocal<User> THREAD_LOCAL = new ThreadLocal<>();

    private UserContext(){}

    public static User get(){
        return THREAD_LOCAL.get();
    }

    public static void set(User user){

        THREAD_LOCAL.remove();
        THREAD_LOCAL.set(user);
    }
}