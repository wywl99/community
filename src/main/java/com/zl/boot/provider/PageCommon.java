package com.zl.boot.provider;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zl.boot.domain.dto.QueryDTO;
import com.zl.boot.domain.vo.PageVO;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;

@Slf4j
public class PageCommon<T> {

    private final int page;

    private final int limit;

    public PageCommon(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }

    public PageCommon(QueryDTO queryVo){
        this.page = queryVo.getPage();
        this.limit = queryVo.getLimit();
    }

    public PageVO<T> paging(QueryTemplate<T> pageTemplate){
        PageHelper.startPage(this.page, this.limit);
        List<T> query = pageTemplate.query();
        PageInfo<T> pageInfo = new PageInfo<>(query);
        PageVO<T> pageResult = new PageVO<>();
        pageResult.setData(pageInfo.getList());
        pageResult.setPage(pageInfo.getPageNum());
        pageResult.setPageCount(pageInfo.getPages());
        pageResult.setTotal(pageInfo.getTotal());

        return pageResult;
    }

    /**
     * 返回一个空分页信息
     */
    public static <V> PageVO<V> emptyPageVO(){
        PageVO<V> emptyPageVO = new PageVO<>();

        emptyPageVO.setData(Collections.emptyList());
        emptyPageVO.setTotal(0L);
        emptyPageVO.setPageCount(0);
        emptyPageVO.setPage(1);

        return emptyPageVO;
    }

    public static <V> PageVO<V> copy(PageVO<?> source,List<V> list){
        PageVO<V> pageVO = new PageVO<>();
        pageVO.setPage(source.getPage());
        pageVO.setTotal(source.getTotal());
        pageVO.setPageCount(source.getPageCount());
        pageVO.setData(list);
        return pageVO;
    }
}