package com.zl.boot.provider;

import java.util.List;

/**
 * @author 向天
 */
@FunctionalInterface
public interface QueryTemplate<T> {

    /**
     * 查询函数，返回查询集合
     * @return 查询的返回集合
     */
    List<T> query();

}
