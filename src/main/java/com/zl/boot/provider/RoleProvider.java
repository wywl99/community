package com.zl.boot.provider;

public class RoleProvider {

    private static final String[] ROLES;

    static {
        ROLES  = new String[]{"user","admin"};
    }

    public static String getRoleValue(int id){
        if (id > ROLES.length){
            throw new IndexOutOfBoundsException("角色信息不存在: " + id);
        }
        return ROLES[id];
    }

}
