package com.zl.boot.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zl.boot.domain.ResultData;
import com.zl.boot.domain.dto.PasswordDTO;
import com.zl.boot.domain.dto.QueryDTO;
import com.zl.boot.domain.dto.RadiusDTO;
import com.zl.boot.domain.vo.CommunityVo;
import com.zl.boot.domain.vo.PageVO;
import com.zl.boot.domain.vo.UserInfoVO;
import com.zl.boot.domain.vo.UserVO;
import com.zl.boot.entity.Community;
import com.zl.boot.entity.User;
import com.zl.boot.provider.PageCommon;
import com.zl.boot.provider.UserContext;
import com.zl.boot.service.ICommunityService;
import com.zl.boot.service.IUserService;
import com.zl.boot.util.PageUtils;
import com.zl.boot.util.ResultUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.geo.Point;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import javax.management.QueryEval;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
@Slf4j
public class UserController {

    private final IUserService userService;
    private final ICommunityService communityService;

    @PostMapping("/page")
    public PageVO<UserVO> getPage(@RequestBody QueryDTO queryDTO){

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getRole,0);

        PageVO<User> paging = new PageCommon<User>(queryDTO).paging(() -> userService.list(queryWrapper));

        Collection<User> data = paging.getData();
        List<UserVO> list = new ArrayList<>(data.size());
        for (User datum : data) {
            UserVO userVO = new UserVO();
            BeanUtil.copyProperties(datum,userVO);

            Community community = communityService.getById(datum.getCommunity());
            userVO.setCommunity0(community);

            list.add(userVO);
        }

        return PageUtils.convert(paging, list);
    }

    @PostMapping("/remove/{id}")
    public ResultData remove(@PathVariable Long id){
        userService.removeById(id);

        return ResultUtils.success();
    }

    @PostMapping("/location/{id}")
    public ResultData relocation(@PathVariable Integer id){
        User user = userService.getById(UserContext.get().getId());
        user.setCommunity(id);
        userService.updateById(user);
        return ResultUtils.success();
    }

    @GetMapping("/info")
    public UserVO getUserInfo(){
        User user = userService.getById(UserContext.get().getId());
        UserVO userVO = new UserVO();
        BeanUtil.copyProperties(user,userVO);
        return userVO;
    }

    @PostMapping("/update")
    public ResultData getUserInfo(@RequestBody User user0){

        User user = userService.getById(UserContext.get().getId());
        user.setNickname(user0.getNickname());
        user.setHeader(user0.getHeader());
        user.setMobile(user0.getMobile());

        userService.updateById(user);

        return ResultUtils.success();
    }

    @PostMapping("/password")
    public ResultData updatePassword(@RequestBody PasswordDTO passwordDTO){

        User user = userService.getById(UserContext.get().getId());

        if (!user.getPassword().equals(passwordDTO.getOld())){
            return ResultUtils.error("当前密码错误");
        }

        if (Strings.isBlank(passwordDTO.getNewPassword())){
            return ResultUtils.error("密码格式错误");
        }

        user.setPassword(passwordDTO.getNewPassword());

        userService.updateById(user);

        return ResultUtils.success();
    }

    @PostMapping("/state")
    public ResultData changeState(Long userId,Integer state){

        User user = userService.getById(userId);

        if (user == null){
            return ResultUtils.error("用户不存在");
        }

        user.setRisk(state);

        userService.updateById(user);

        return ResultUtils.success();
    }
}
