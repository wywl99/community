package com.zl.boot.controller;

import com.zl.boot.domain.ResultData;
import com.zl.boot.domain.dto.QueryDTO;
import com.zl.boot.domain.vo.PageVO;
import com.zl.boot.entity.Vaccine;
import com.zl.boot.provider.PageCommon;
import com.zl.boot.service.IVaccineService;
import com.zl.boot.util.ResultUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhanglun
 * @since 2022-04-26
 */
@RestController
@RequestMapping("/vaccine")
@Slf4j
@AllArgsConstructor
public class VaccineController {

    private final IVaccineService vaccineService;

    @PostMapping("/page")
    public PageVO<Vaccine> getPage(@RequestBody QueryDTO queryDTO){

        return new PageCommon<Vaccine>(queryDTO).paging(vaccineService::list);
    }

    /**
     * 保存疫苗预约信息
     */
    @PostMapping
    public ResultData save(@RequestBody Vaccine vaccine){
        vaccineService.save(vaccine);
        return ResultUtils.success();
    }

    @PostMapping("/remove/{id}")
    public ResultData remove(@PathVariable Long id){
        vaccineService.removeById(id);
        return ResultUtils.success();
    }

    @PostMapping("/update")
    public ResultData update(@RequestBody Vaccine vaccine){
        vaccineService.updateById(vaccine);
        return ResultUtils.success();
    }

    @GetMapping("/{id}")
    public ResultData getOne(@PathVariable Long id){
        Vaccine vaccine = vaccineService.getById(id);
        return ResultUtils.success(vaccine);
    }
}