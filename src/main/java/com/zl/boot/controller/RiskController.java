package com.zl.boot.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zl.boot.domain.dto.RadiusDTO;
import com.zl.boot.domain.vo.CommunityRiskInfo;
import com.zl.boot.domain.vo.NoticeVO;
import com.zl.boot.entity.Community;
import com.zl.boot.entity.User;
import com.zl.boot.provider.GeoDistProvider;
import com.zl.boot.provider.UserContext;
import com.zl.boot.service.ICommunityService;
import com.zl.boot.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.geo.Point;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/risk")
public class RiskController {

    private final ICommunityService communityService;
    private final IUserService userService;
    private final GeoDistProvider geoDistProvider;

    /**
     * 获取自己社区的疫情防控等级
     * @return
     */
    @GetMapping
    public CommunityRiskInfo getRiskInfo(){

        User user = userService.getById(UserContext.get().getId());

        if (user.getCommunity() == null){
            return null;
        }

        Community community = communityService.getById(user.getCommunity());

        CommunityRiskInfo communityRiskInfo = new CommunityRiskInfo();
        BeanUtil.copyProperties(community,communityRiskInfo);

        //获取该小区的人数
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getCommunity,community.getId());

        communityRiskInfo.setPersonCount((int) userService.count(queryWrapper));

        communityRiskInfo.setVaccine(user.getVaccineState() == 2);
        return communityRiskInfo;
    }

    /**
     * 疫情风险通知
     */
    @GetMapping("/notice")
    public List<NoticeVO> notice(){

        //获取用户小区
        User user = userService.getById(UserContext.get().getId());
        Community community0 = communityService.getById(user.getCommunity());

        List<RadiusDTO> radius = geoDistProvider.getByRadius(new Point(community0.getLongitude(), community0.getLatitude()));

        List<NoticeVO> list = new ArrayList<>(radius.size());
        for (RadiusDTO radiusDTO : radius) {
            Community community = communityService.getById(radiusDTO.getId());
            if (community.getRisk() == 0) continue;;
            NoticeVO noticeVO = new NoticeVO();
            noticeVO.setCommunity(community);
            noticeVO.setDist(radiusDTO.getDist());
            list.add(noticeVO);
        }

        return list.stream().sorted((o1, o2) -> o2.getDist() < o1.getDist() ? 0 : 1).collect(Collectors.toList());
    }

}