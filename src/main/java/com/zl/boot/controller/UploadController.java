package com.zl.boot.controller;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.file.FileNameUtil;
import com.zl.boot.domain.ResultData;
import com.zl.boot.util.ResultUtils;
import com.zl.boot.util.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传
 */
@Controller
@Slf4j
public class UploadController {

    @PostMapping("/upload/img")
    @ResponseBody
    public ResultData upload(@RequestParam("file") MultipartFile file) throws IOException {

        log.info("上传的文件名是:{}",file.getOriginalFilename());

        String suffix = FileNameUtil.extName(file.getOriginalFilename());
        String uuid = UUID.randomUUID() + "." + suffix;

        //获取当前路径
        String path = ResourceUtils.getURL("classpath:").getPath();

        FileOutputStream outputStream = new FileOutputStream(path + '/' + uuid);
        IOUtils.copy(file.getInputStream(),outputStream);
        outputStream.close();
        file.getInputStream().close();


        return ResultUtils.success("/img/" + uuid);
    }

    /**
     * 下载图片
     */
    @GetMapping("/img/{filename}")
    public void download(@PathVariable String filename) throws IOException {

        String path = ResourceUtils.getURL("classpath:").getPath();

        File file = new File(path + '/' + filename);
        if (!file.exists()){
            log.warn(file.getAbsolutePath() + "不存在");
            return;
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        HttpServletResponse response = SpringUtils.getResponse();
        response.setHeader("Content-Type","image/jpeg");
        IoUtil.copy(fileInputStream, response.getOutputStream());
        fileInputStream.close();
    }
}