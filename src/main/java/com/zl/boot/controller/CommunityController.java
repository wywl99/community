package com.zl.boot.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zl.boot.domain.MyPoint;
import com.zl.boot.domain.ResultData;
import com.zl.boot.domain.dto.QueryDTO;
import com.zl.boot.domain.dto.RadiusDTO;
import com.zl.boot.domain.vo.CommunityVo;
import com.zl.boot.domain.vo.PageVO;
import com.zl.boot.entity.Community;
import com.zl.boot.entity.User;
import com.zl.boot.entity.Vaccine;
import com.zl.boot.provider.GeoDistProvider;
import com.zl.boot.provider.PageCommon;
import com.zl.boot.provider.UserContext;
import com.zl.boot.service.ICommunityService;
import com.zl.boot.service.IUserService;
import com.zl.boot.util.PageUtils;
import com.zl.boot.util.ResultUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
@RestController
@RequestMapping("/community")
@AllArgsConstructor
@Slf4j
public class CommunityController {

    private final ICommunityService communityService;
    private final GeoDistProvider geoDistProvider;
    private final IUserService userService;
    private final StringRedisTemplate redisTemplate;

    @PostMapping("/page")
    public PageVO<CommunityVo> getPage(@RequestBody QueryDTO queryDTO){

        LambdaQueryWrapper<Community> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(Community::getName,"%" + queryDTO.getKeyword() +"%");

        PageVO<Community> paging = new PageCommon<Community>(queryDTO).paging(() -> communityService.list(lambdaQueryWrapper));

        List<CommunityVo> collect = paging.getData().stream().map(item -> {

            CommunityVo communityVo = new CommunityVo();

            BeanUtil.copyProperties(item, communityVo);

            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getCommunity, item.getId());

            long count = userService.count(queryWrapper);

            communityVo.setNum((int) count);
            return communityVo;
        }).collect(Collectors.toList());

        return PageUtils.convert(paging,collect);
    }

    @PostMapping
    public ResultData save(@RequestBody Community community){
        communityService.save(community);

        Point point = new Point(community.getLongitude(),community.getLatitude());
        geoDistProvider.addPoint(point,community.getId());
        return ResultUtils.success();
    }

    @PostMapping("/remove/{id}")
    public ResultData remove(@PathVariable Long id){
        communityService.removeById(id);
        return ResultUtils.success();
    }

    @PostMapping("/update")
    public ResultData update(@RequestBody Community community){
        communityService.updateById(community);
        return ResultUtils.success();
    }

    @GetMapping("/{id}")
    public ResultData getOne(@PathVariable Long id){
        Community community = communityService.getById(id);
        return ResultUtils.success(community);
    }

    @GetMapping("/location")
    public MyPoint getMyLocation(){

        User user = UserContext.get();

        user = userService.getById(user.getId());

        if (user.getCommunity() == null){
            return null;
        }

        Community community = communityService.getById(user.getCommunity());

        if (community == null) return null;

        return new MyPoint(community.getLongitude(),community.getLatitude(),community.getName());
    }

    @GetMapping("/relocation")
    public List<CommunityVo> relocation(MyPoint myPoint){

        List<RadiusDTO> byRadius = geoDistProvider.getByRadius(new Point(myPoint.getLongitude(), myPoint.getLatitude()));

        List<CommunityVo> collect = byRadius.stream().map(item -> {
            Community community = communityService.getById(item.getId());
            CommunityVo communityVo = new CommunityVo();
            BeanUtil.copyProperties(community, communityVo);
            communityVo.setDist(item.getDist());
            return communityVo;
        }).collect(Collectors.toList());

        return collect;
    }

}