package com.zl.boot.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zl.boot.domain.ResultData;
import com.zl.boot.domain.dto.UserRegisterDTO;
import com.zl.boot.domain.vo.UserInfoVO;
import com.zl.boot.domain.vo.UserVO;
import com.zl.boot.entity.Community;
import com.zl.boot.entity.User;
import com.zl.boot.service.ICommunityService;
import com.zl.boot.service.IUserService;
import com.zl.boot.util.JwtUtils;
import com.zl.boot.util.ResultUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
@Slf4j
public class LoginController {

    private final IUserService userService;
    private final ICommunityService communityService;
    private final StringRedisTemplate redisTemplate;

    /**
     * 默认头像
     */
    private static final String DEFAULT_HEADER_URL = "https://s3.bmp.ovh/imgs/2022/05/07/adbebedb9f137dd7.webp";

    @PostMapping("/login")
    public ResultData login(@RequestBody UserRegisterDTO userDTO){

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUsername,userDTO.getUsername())
                        .eq(User::getPassword,userDTO.getPassword());

        User one = userService.getOne(queryWrapper);

        if (one == null){
            return ResultUtils.error("账号或者密码错误");
        }

        if (userDTO.getAdmin() && one.getRole() != 1){
            return ResultUtils.error("当前用户非管理员");
        }

        if (!userDTO.getAdmin() && one.getRole() != 0){
            return ResultUtils.error("账号或者密码错误");
        }

        String role = one.getRole() == 1 ? "admin" : "normal";

        //生成一个token
        HashMap<String, Object> tokenInfo = new HashMap<>();
        tokenInfo.put("id",one.getId());
        tokenInfo.put("username",one.getUsername());
        tokenInfo.put("role",one.getRole() == 1 ? "admin" : "normal");

        String token = JwtUtils.getToken(tokenInfo);

        UserInfoVO userInfoVO = new UserInfoVO();

        UserVO userVO = new UserVO();
        BeanUtil.copyProperties(one,userVO);

        userInfoVO.setUserInfo(userVO);
        userInfoVO.setToken(token);
        userInfoVO.setRoles(Collections.singletonList(role));

        return ResultUtils.success(userInfoVO);
    }

    @PostMapping("/register")
    public ResultData register(@RequestBody UserRegisterDTO userDTO){

        User user = new User();
        BeanUtil.copyProperties(userDTO,user);
        user.setId(null);
        user.setTime(LocalDateTime.now());
        userService.save(user);

        return ResultUtils.success();
    }

    @GetMapping("/fill")
    public ResultData fillToRedis(){
        for (Community community : communityService.list()) {
            Point point = new Point(community.getLongitude(), community.getLatitude());
            redisTemplate.opsForGeo().add("city",point, UUID.randomUUID().toString().replaceAll("-","") + "-" + community.getId());
        }

        return ResultUtils.success();
    }

}