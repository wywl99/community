package com.zl.boot.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zl.boot.domain.ResultData;
import com.zl.boot.domain.dto.QueryDTO;
import com.zl.boot.domain.dto.SubscribeDTO;
import com.zl.boot.domain.vo.PageVO;
import com.zl.boot.domain.vo.PeriodVO;
import com.zl.boot.domain.vo.SubscribeVO;
import com.zl.boot.domain.vo.UserVO;
import com.zl.boot.entity.Subscribe;
import com.zl.boot.entity.User;
import com.zl.boot.entity.Vaccine;
import com.zl.boot.provider.PageCommon;
import com.zl.boot.provider.UserContext;
import com.zl.boot.service.ISubscribeService;
import com.zl.boot.service.IUserService;
import com.zl.boot.service.IVaccineService;
import com.zl.boot.util.PageUtils;
import com.zl.boot.util.ResultUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
@RestController
@RequestMapping("/subscribe")
@Slf4j
@AllArgsConstructor
public class SubscribeController {

    private final ISubscribeService subscribeService;
    private final IUserService userService;

    private final IVaccineService vaccineService;

    @PostMapping("/page")
    public PageVO<SubscribeVO> page(@RequestBody QueryDTO queryDTO){

        LambdaQueryWrapper<Subscribe> queryWrapper = new LambdaQueryWrapper<>();

        User user = UserContext.get();
        user = userService.getById(user.getId());

        if (queryDTO.getKeyword() != null){
            queryWrapper.like(Subscribe::getCard,"%" + queryDTO.getKeyword() + "%");
        }

        PageVO<Subscribe> paging = new PageCommon<Subscribe>(queryDTO).paging(() -> subscribeService.list(queryWrapper));

        List<SubscribeVO> collect = paging.getData().stream().map(item -> {

            Vaccine vaccine = vaccineService.getById(item.getVaccineId());
            User user0 = userService.getById(item.getUser());

            UserVO userVO = new UserVO();
            BeanUtil.copyProperties(user0, userVO);

            SubscribeVO subscribeVO = new SubscribeVO();
            subscribeVO.setUser(userVO);
            subscribeVO.setVaccine(vaccine);

            subscribeVO.setTime(item.getTime());
            subscribeVO.setState(item.getState());
            subscribeVO.setId(item.getId());

            return subscribeVO;
        }).collect(Collectors.toList());

        return PageUtils.convert(paging,collect);
    }

    @PostMapping
    public ResultData save(@RequestBody SubscribeDTO subscribeDTO){

        Subscribe subscribe = new Subscribe();
        subscribe.setState(0);

        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(subscribeDTO.getTime().toInstant(), zoneId);
        subscribe.setTime(localDateTime);

        subscribe.setVaccineId(subscribeDTO.getVaccineId());
        subscribe.setUser(UserContext.get().getId());

        User user = userService.getById(UserContext.get().getId());

        subscribe.setCard(user.getCard());

        subscribeService.save(subscribe);

        user.setVaccineState(1);
        userService.updateById(user);

        return ResultUtils.success();
    }

    @PostMapping("/audit/{id}")
    public ResultData audit(@PathVariable Long id){

        LambdaUpdateWrapper<Subscribe> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(Subscribe::getState,1).eq(Subscribe::getId,id);
        subscribeService.update(updateWrapper);

        Subscribe subscribe = subscribeService.getById(id);

        User user = userService.getById(subscribe.getUser());
        user.setVaccineState(user.getVaccineState() + 1);
        userService.updateById(user);

        return ResultUtils.success();
    }

    @GetMapping("/info")
    public Subscribe getInfo(){
        LambdaQueryWrapper<Subscribe> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Subscribe::getUser,UserContext.get().getId())
                .orderByDesc(Subscribe::getTime);
        List<Subscribe> list = subscribeService.list(queryWrapper);
        if (list.isEmpty()){
            return null;
        }
        return list.get(0);
    }

    @PostMapping("/cancel")
    public ResultData cancel(){

        LambdaQueryWrapper<Subscribe> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Subscribe::getUser,UserContext.get().getId())
                .orderByDesc(Subscribe::getId);

        Subscribe subscribe = subscribeService.list(queryWrapper).get(0);

        if (subscribe.getState() == 1){
            return ResultUtils.error("疫苗已接种，不能取消");
        }

        subscribeService.removeById(subscribe.getId());
        return ResultUtils.success();
    }

    @GetMapping("/user")
    public List<Subscribe> getInfoByUser(){

        LambdaQueryWrapper<Subscribe> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Subscribe::getUser,UserContext.get().getId())
                .eq(Subscribe::getState,1);

        return subscribeService.list(queryWrapper);
    }

    @GetMapping("/is_next")
    public Boolean isNext(){

        LambdaQueryWrapper<Subscribe> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Subscribe::getState,0).eq(Subscribe::getUser,UserContext.get().getId());

        long count = subscribeService.count(queryWrapper);
        return count == 0;
    }

    private static final long ONE_DAY_MILLISECOND = 24 * 60 * 60 * 1000;

    @GetMapping("/period")
    public PeriodVO period(){

        Long userId = UserContext.get().getId();

        LambdaQueryWrapper<Subscribe> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Subscribe::getUser,userId)
                .eq(Subscribe::getState,1)
                .orderByDesc(Subscribe::getTime);

        List<Subscribe> list = subscribeService.list(queryWrapper);

        if (list.isEmpty()){
            return new PeriodVO(0,0L);
        }

        Subscribe subscribe = list.get(0);

        Vaccine vaccine = vaccineService.getById(subscribe.getVaccineId());

        long times = getTime(LocalDateTime.now()) - getTime(subscribe.getTime());

        long interval = vaccine.getPeriod() - times;

        long day = times / ONE_DAY_MILLISECOND;

        //小于剩余失效的三分之一就需要提示
        if (interval / ONE_DAY_MILLISECOND < (vaccine.getPeriod() / ONE_DAY_MILLISECOND) / 3){
            return new PeriodVO(list.size()+1,day);
        }

        return new PeriodVO(0,0L);
    }

    private Long getTime(LocalDateTime localDateTime){
        return localDateTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

}