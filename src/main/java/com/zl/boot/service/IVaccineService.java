package com.zl.boot.service;

import com.zl.boot.entity.Vaccine;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
public interface IVaccineService extends IService<Vaccine> {

}
