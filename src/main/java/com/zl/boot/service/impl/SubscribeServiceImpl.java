package com.zl.boot.service.impl;

import com.zl.boot.entity.Subscribe;
import com.zl.boot.mapper.OrderMapper;
import com.zl.boot.service.ISubscribeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
@Service
public class SubscribeServiceImpl extends ServiceImpl<OrderMapper, Subscribe> implements ISubscribeService {

}
