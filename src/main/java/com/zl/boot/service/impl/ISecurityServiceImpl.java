package com.zl.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zl.boot.domain.vo.CommunityVo;
import com.zl.boot.domain.vo.SecurityInfo;
import com.zl.boot.entity.Community;
import com.zl.boot.entity.User;
import com.zl.boot.service.ICommunityService;
import com.zl.boot.service.ISecurityService;
import com.zl.boot.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ISecurityServiceImpl implements ISecurityService {

    private final ICommunityService communityService;
    private final IUserService userService;

    /**
     * 整点半小时统计一次
     */
    @Scheduled(cron = "0/10 0 * * * *")
    @Override
    public void getSecurity() {

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();

        List<Community> communities = communityService.list();
        for (Community community : communities) {

            queryWrapper.eq(User::getCommunity,community.getId())
                    .eq(User::getRisk ,1);
            //密接人数
            long count = userService.count(queryWrapper);

            queryWrapper.clear();
            queryWrapper.eq(User::getCommunity,community.getId())
                            .eq(User::getRisk,2);
            community.setContact((int) count);

            long count0 = userService.count(queryWrapper);
            community.setConfirmed((int) count0);

            if (count0 >= 10){
                community.setRisk(2);
            }else if (count0 >= 1){
                community.setRisk(1);
            }else {
                community.setRisk(0);
            }

            communityService.updateById(community);

        }
        log.info("统计数据完成 --- {}",new SimpleDateFormat("HH:mm:ss").format(new Date()));

    }
}
