package com.zl.boot.service.impl;

import com.zl.boot.entity.Vaccine;
import com.zl.boot.mapper.VaccineMapper;
import com.zl.boot.service.IVaccineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhanglun
 * @since 2022-04-26
 */
@Service
public class VaccineServiceImpl extends ServiceImpl<VaccineMapper, Vaccine> implements IVaccineService {

}
