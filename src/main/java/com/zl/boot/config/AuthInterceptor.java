package com.zl.boot.config;

import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zl.boot.entity.User;
import com.zl.boot.provider.UserContext;
import com.zl.boot.util.JwtUtils;
import com.zl.boot.util.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;

@Slf4j
public class AuthInterceptor implements HandlerInterceptor {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String token = request.getHeader("token");

        response.setContentType("application/json;charset=utf-8");
        if (Strings.isBlank(token)){
            response.getWriter().println(objectMapper.writeValueAsString(ResultUtils.error(401,"未登录!")));
            return false;
        }

        if (!JWTUtil.verify(token,JwtUtils.SALT.getBytes(StandardCharsets.UTF_8))){
            response.getWriter().println(objectMapper.writeValueAsString(ResultUtils.error(401,"令牌错误，请重新登录!")));
            return false;
        }

        JWT jwt = JWTUtil.parseToken(token);

        JSONObject jsonObject = jwt.getPayloads();

        User user = new User();
        user.setId(jsonObject.getLong("id"));
        user.setUsername(jsonObject.getStr("username"));

        UserContext.set(user);

        return true;
    }
}
