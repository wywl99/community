package com.zl.boot.config;

import com.zl.boot.domain.ResultData;
import com.zl.boot.util.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 异常处理
 *
 */
@RestControllerAdvice
@Slf4j
public class CommonExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResultData handleGlobalException(Exception e){
        e.printStackTrace();
        return ResultUtils.error(500,"服务器出错了");
    }
}