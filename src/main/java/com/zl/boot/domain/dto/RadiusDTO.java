package com.zl.boot.domain.dto;

import lombok.Data;

@Data
public class RadiusDTO {

    private Integer id;

    private Double dist;

    private String unit;

}
