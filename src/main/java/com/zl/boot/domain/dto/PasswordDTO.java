package com.zl.boot.domain.dto;

import lombok.Data;

/**
 * 修改密码
 */
@Data
public class PasswordDTO {

    private String old;

    private String newPassword;
}
