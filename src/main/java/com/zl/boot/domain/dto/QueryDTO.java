package com.zl.boot.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 查询条件
 *
 */
@Data
public class QueryDTO implements Serializable {

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 当前页
     */
    private Integer page;

    /**
     * 每页显示的数量
     */
    private Integer limit;

    /**
     * 状态
     */
    private Integer state;
}