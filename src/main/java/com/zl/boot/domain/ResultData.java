package com.zl.boot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultData {

    /**
     * 返回的状态码
     */
    private int code;

    /**
     * 返回的消息
     */
    private String msg;

    /**
     * 返回的数据
     */
    private Object data;
}
