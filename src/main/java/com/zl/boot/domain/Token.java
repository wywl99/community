package com.zl.boot.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 令牌
 *
 */
@Data
public class Token implements Serializable {

    private String username;

    private Long expirationTime;

    private Long releaseTime;

    private String nickname;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long playerId;
}