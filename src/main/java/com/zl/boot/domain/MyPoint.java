package com.zl.boot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyPoint {

    private Double longitude;
    private Double latitude;

    private String name;
}
