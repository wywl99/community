package com.zl.boot.domain.vo;

import lombok.Data;

import java.util.Collection;
import java.util.Collections;

/**
 *
 */
@Data
public class PageVO<T> {

    /**
     * 当前页
     */
    private Integer page;

    /**
     * 每一页的数量
     */
    private final Integer limit = 10;

    /**
     * 总数据条数
     */
    private Long total;

    /**
     * 一共有多少页
     */
    private Integer pageCount;

    /**
     * 分页的数据
     */
    private Collection<T> data = Collections.emptyList();
}
