package com.zl.boot.domain.vo;

import com.zl.boot.entity.Community;
import lombok.Data;

@Data
public class SecurityInfo {

    private Community community;

    /**
     * 0 低风险
     * 1 中风险  有一个确诊的就是中风险
     * 2 高风险  有十个或以上就是高风险
     */
    private Integer risk;

}