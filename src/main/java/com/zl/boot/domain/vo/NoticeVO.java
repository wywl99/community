package com.zl.boot.domain.vo;

import com.zl.boot.entity.Community;
import lombok.Data;

@Data
public class NoticeVO {

    /**
     * 社区
     */
    private Community community;

    private Double dist;
}
