package com.zl.boot.domain.vo;

import lombok.Data;

@Data
public class CommunityVo {

    private Integer id;

    private String name;

    /**
     * 经度

     */
    private Double longitude;

    /**
     * 纬度
     */
    private Double latitude;

    /**
     * 所有人数
     */
    private Integer num;

    /**
     * 当前距离
     */
    private Double dist;
}
