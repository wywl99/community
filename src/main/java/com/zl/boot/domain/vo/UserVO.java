package com.zl.boot.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zl.boot.entity.Community;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.awt.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class UserVO implements Serializable {

    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 名称
     */
    private String nickname;


    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime time;

    /**
     * 风险等级
     */
    private Integer risk;

    /**
     * 社区信息
     */
    private Community community0;

    private String card;

    private String realName;

    private String role;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 头像地址
     */
    private String header;

    /**
     * 疫苗接种情况
     */
    private Integer vaccineState;
}