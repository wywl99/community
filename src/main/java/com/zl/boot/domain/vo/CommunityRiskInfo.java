package com.zl.boot.domain.vo;

import lombok.Data;

/**
 * 社区风险计算
 */
@Data
public class CommunityRiskInfo {

    private Integer personCount;

    /**
     * 风险等级
     */
    private Integer risk;

    /**
     * 密接人数
     */
    private Integer contact;

    /**
     * 确诊人数
     */
    private Integer confirmed;

    /**
     * 是否接种疫苗
     */
    private Boolean vaccine;

}