package com.zl.boot.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zl.boot.entity.Vaccine;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class SubscribeVO {

    private Long id;

    private UserVO user;

    private Vaccine vaccine;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime time;

    private Integer state;
}
