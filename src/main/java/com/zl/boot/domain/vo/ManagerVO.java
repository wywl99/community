package com.zl.boot.domain.vo;

import lombok.Data;

@Data
public class ManagerVO {

    private Long id;

    private String username;

    private String nickname;

    private String password;

    private String token;
}
