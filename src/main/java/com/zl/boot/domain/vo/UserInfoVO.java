package com.zl.boot.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserInfoVO {

    /**
     * 拥有的角色
     */
    private List<String> roles;

    /**
     * 令牌
     */
    private String token;

    /**
     * 用户信息
     */
    private UserVO userInfo;

    /**
     * 令牌
     */
    private String adminToken;

}
