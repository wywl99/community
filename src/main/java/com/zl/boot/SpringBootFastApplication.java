package com.zl.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan(basePackages = "com.zl.boot.mapper")
@EnableScheduling
@SpringBootApplication
public class SpringBootFastApplication{

    public static void main(String[] args) {
        SpringApplication.run(SpringBootFastApplication.class, args);
    }
}
