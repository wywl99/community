package cn.wywl.springbootfast;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.zl.boot.domain.MyPoint;
import com.zl.boot.provider.GeoDistProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Point;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@SpringBootTest
class SpringBootFastApplicationTests {

    @Test
    void contextLoads() {

        FastAutoGenerator.create("jdbc:mysql://114.132.225.88:3306/community?serverTimezone=Asia/Shanghai&autoReconnect=true&useSSL=false", "root", "lky13589.....")
                .globalConfig(builder -> {
                    builder.author("zhanglun") // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D:\\Code\\huyaoding\\SpringBoot-fast\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.zl") // 设置父包名
                            .moduleName("boot") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapper, "D:\\Code\\huyaoding\\SpringBoot-fast\\src\\main\\resources")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("manager"); // 设置需要生成的表名
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

    public static List<String> getTables(String tables) {
        return "all".equals(tables) ? Collections.emptyList() : Arrays.asList(tables.split(","));
    }

}
